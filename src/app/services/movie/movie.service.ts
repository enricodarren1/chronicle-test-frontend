import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  API_KEY = 'b37834cd26e6517971d26f4e986ebe11';

  constructor(private readonly httpClient: HttpClient) { }

  getDiscoverMovie(params: any) {
    return this.httpClient.get(`https://api.themoviedb.org/3/discover/movie?api_key=${this.API_KEY}&include_adult=false&include_video=false&page=1&with_watch_monetization_types=flatrate&language=en-US`, {
      params: params
    });
  }

  getGenreMovie() {
    return this.httpClient.get(`https://api.themoviedb.org/3/genre/movie/list?api_key=${this.API_KEY}&language=en-US`);
  }

  getMovieById(id: number) {
    return this.httpClient.get(`https://api.themoviedb.org/3/movie/${id}?api_key=${this.API_KEY}&language=en-US`)
  }
}
