import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MovieService } from './movie/movie.service';



@NgModule({
  declarations: [],
  providers: [MovieService],
  imports: [
    CommonModule,
  ]
})
export class ServiceModule { }
