import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HomeComponent } from './home/home.component';
import { MovieDetailComponent } from './movie-detail/movie-detail.component';


import { PipesModule } from '../pipes/pipes.module';
import { ComponentModule } from '../components/component.module';

import { AngularMaterialModule } from '../angular-material/angular-material.module';
import { MovieFavoritesComponent } from './movie-favorites/movie-favorites.component';

const pages = [
  HomeComponent,
  MovieDetailComponent,
  MovieFavoritesComponent
]

@NgModule({
  declarations: [pages],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    ComponentModule,
    PipesModule,
    AngularMaterialModule
  ],
  exports: [pages]
})
export class PageModule { }
