import { HttpClient, HttpHandler } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { of } from 'rxjs';

import { MovieDetailComponent } from './movie-detail.component';
import { MatIconModule } from '@angular/material/icon';
import { GetImagePipe } from 'src/app/pipes/imagePipe/get-image.pipe';
import { MovieService } from 'src/app/services/movie/movie.service';

describe('MovieDetailComponent', () => {
  let component: MovieDetailComponent;
  let fixture: ComponentFixture<MovieDetailComponent>;
  let movieService: MovieService;
  const fakeActivatedRoute = {
    snapshot: { data: {} }
  } as ActivatedRoute

  const mockData: any = [
    {
      title: 'test',
      runtime_hours: '',
      runtime_minutes: ''
    }
  ]

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MovieDetailComponent, GetImagePipe],
      imports: [MatIconModule],
      providers: [HttpClient, HttpHandler, { provide: ActivatedRoute, useValue: fakeActivatedRoute }]
    })
      .compileComponents();

    movieService = TestBed.inject(MovieService);
    fixture = TestBed.createComponent(MovieDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('it should navigate to detail', () => {
    component.goBack();
    expect(component.goBack()).toEqual();
  });

  it('it should get data movie detail', () => {
    spyOn(movieService, 'getMovieById').and.returnValue(of(mockData));
    component.getMovieDetail(12);
    expect(component.movieDetail).toBeTruthy();
  });
});
