import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MovieService } from 'src/app/services/movie/movie.service';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.scss']
})
export class MovieDetailComponent {

  movieDetail: any;

  constructor(
    private readonly movieService: MovieService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router
  ) {
    this.activatedRoute.params?.subscribe((data: any) => {
      this.getMovieDetail(data.id);
    })
  }

  goBack() {
    this.router.navigate(['/home']);
  }

  getMovieDetail(movieId: number) {
    this.movieService.getMovieById(movieId).subscribe((data: any) => {
      data.runtime_hours = Math?.floor(data?.runtime / 60);
      data.runtime_minutes = data?.runtime % 60;
      this.movieDetail = data;
    })
  }
}
