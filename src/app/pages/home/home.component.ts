import { HttpParams } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { MovieService } from 'src/app/services/movie/movie.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {

  movieForm: FormGroup | any;

  orderItemList = [
    {
      id: 0,
      name: 'Popularity',
      value: 'popularity'
    },
    {
      id: 1,
      name: 'Release Date',
      value: 'primary_release_date'
    },
    {
      id: 2,
      name: 'Vout Count',
      value: 'vount_count'
    }
  ];
  sortItemList = [
    {
      id: 0,
      name: 'Ascending',
      value: 'asc'
    },
    {
      id: 1,
      name: 'Descending',
      value: 'desc'
    }
  ]
  movieList = [];
  movieListStored = [];
  genreList = [];

  constructor(
    private readonly fb: FormBuilder,
    private readonly movieService: MovieService,
    private readonly router: Router
  ) {
    this.createForm();

    this.fetchGenreList();
    this.fetchMovieList();
  }

  createForm() {
    this.movieForm = this.fb.group({
      releaseFrom: new FormControl(new Date()),
      releaseTo: new FormControl(new Date()),
      orderBy: new FormControl('popularity'),
      sortBy: new FormControl('desc')
    })
  }

  fetchMovieList() {
    const formValue = this.movieForm.value;
    const dateFrom = moment(formValue.releaseFrom).format('YYYY-MM-DD');
    const dateTo = moment(formValue.releaseTo).format('YYYY-MM-DD');
    let params = new HttpParams()
      .set('sort_by', `${formValue.orderBy}.${formValue.sortBy}`)
      .set('primary_release_date.gte', dateFrom)
      .set('primary_release_date.lte', dateTo)
    this.movieService.getDiscoverMovie(params).subscribe((res: any) => {
      const movieListStored = localStorage.getItem('favorite-movie-list') ? JSON.parse(localStorage.getItem('favorite-movie-list') || '') : [];
      res.results.forEach((element: any) => {
        if (movieListStored.length > 0) {
          movieListStored.some((data: any) => {
            if (data.id === element.id) {
              element.favorite = data.favorite;
            }
          });
        } else {
          element.favorite = false;
        }
      });
      this.movieList = res.results;
      localStorage.setItem('favorite-movie-list', JSON.stringify(this.movieList));
    });

  }

  fetchGenreList() {
    this.movieService.getGenreMovie().subscribe((res: any) => {
      this.genreList = res.genres;
    });
  }

  gotoFavoriteMovie() {
    localStorage.setItem('favorite-movie-list', JSON.stringify(this.movieList));
    this.router.navigate(['/favorite-movie']);
  }
}
