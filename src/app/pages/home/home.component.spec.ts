import { Location } from '@angular/common';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepicker, MatDatepickerModule } from '@angular/material/datepicker';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { MovieService } from 'src/app/services/movie/movie.service';

import { HomeComponent } from './home.component';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let movieService: MovieService;
  let router: Router;
  let location: Location;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeComponent ],
      imports: [MatExpansionModule, MatFormFieldModule, MatDatepickerModule, MatNativeDateModule, MatSelectModule, BrowserAnimationsModule, ReactiveFormsModule],
      providers: [HttpClient, HttpHandler, MatDatepicker]
    })
    .compileComponents();

    movieService = TestBed.inject(MovieService);
    router = TestBed.inject(Router);
    location = TestBed.get(Location);
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('it should get data genre list', () => {
    const fakeValue = [
      {
        title: ''
      }
    ]
    
    spyOn(movieService, 'getGenreMovie').and.returnValue(of(fakeValue));
    component.fetchGenreList();
    expect(component.genreList).toBeFalsy();
  });

  it('it should navigate to detail', () => {
    component.gotoFavoriteMovie();
    router.navigate(['/home']);
    expect(location.path()).toEqual('');
  });
});
