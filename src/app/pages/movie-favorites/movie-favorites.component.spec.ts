import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatIconModule } from '@angular/material/icon';

import { MovieFavoritesComponent } from './movie-favorites.component';

describe('MovieFavoritesComponent', () => {
  let component: MovieFavoritesComponent;
  let fixture: ComponentFixture<MovieFavoritesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MovieFavoritesComponent ],
      imports: [MatIconModule, HttpClientModule]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MovieFavoritesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
