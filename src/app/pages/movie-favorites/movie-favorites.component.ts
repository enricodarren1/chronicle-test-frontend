import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MovieService } from 'src/app/services/movie/movie.service';

@Component({
  selector: 'app-movie-favorites',
  templateUrl: './movie-favorites.component.html',
  styleUrls: ['./movie-favorites.component.scss']
})
export class MovieFavoritesComponent {

  movieList:any = [];
  movieListFiltered: any = [];
  genreList = [];

  constructor(
    private readonly router: Router,
    private readonly movieService: MovieService
  ) {
    const movieListStored = localStorage.getItem('favorite-movie-list');
    this.movieListFiltered = movieListStored && JSON.parse(movieListStored || '').filter((data: any) => data.favorite);
    this.movieList = movieListStored && JSON.parse(movieListStored || '');

    this.fetchGenreList();
  }

  fetchGenreList() {
    this.movieService.getGenreMovie().subscribe((res: any) => {
      this.genreList = res.genres;
    });
  }

  goBack() {
    this.router.navigate(['/home']);
    localStorage.setItem('favorite-movie-list', JSON.stringify(this.movieList));
  }

}
