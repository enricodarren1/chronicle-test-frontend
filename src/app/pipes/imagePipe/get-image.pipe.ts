import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'getImage'
})
export class GetImagePipe implements PipeTransform {

  transform(value: any, ...args: unknown[]): any {
    if (value !== null) {
      return `https://image.tmdb.org/t/p/original/${value}`;
    } else {
      return 'https://dummyimage.com/306x459/000/fff.png&text=Placeholder+Image'
    }
  }

}
