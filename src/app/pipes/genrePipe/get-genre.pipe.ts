import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'getGenre'
})
export class GetGenrePipe implements PipeTransform {

  transform(value: any, ...args: any[]): unknown {
    let genreList: any = [];
    args[0].forEach((genre: any) => {
      value.some((movieGenre: any, index: any) => {
        if (genre.id === movieGenre) {
          genreList.push(genre.name);
          return genreList;
        }
      });
    });
    return genreList;
  }

}
