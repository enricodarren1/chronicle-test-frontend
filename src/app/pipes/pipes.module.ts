import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GetGenrePipe } from './genrePipe/get-genre.pipe';
import { GetImagePipe } from './imagePipe/get-image.pipe';



@NgModule({
  declarations: [GetGenrePipe, GetImagePipe],
  imports: [
    CommonModule
  ],
  exports: [GetGenrePipe, GetImagePipe]
})
export class PipesModule { }
