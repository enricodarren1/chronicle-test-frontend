import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MovieCardComponent } from './movie-card/movie-card.component';
import { MatCardModule } from '@angular/material/card';
import { PipesModule } from '../pipes/pipes.module';
import { AngularMaterialModule } from '../angular-material/angular-material.module';

const components = [
  MovieCardComponent
]

@NgModule({
  declarations: [components],
  imports: [
    CommonModule,
    PipesModule,
    AngularMaterialModule
  ],
  exports: [components]
})
export class ComponentModule { }
