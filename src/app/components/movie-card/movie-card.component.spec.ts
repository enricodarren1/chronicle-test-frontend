import { Location } from '@angular/common';
import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { MatCardModule } from '@angular/material/card';
import { Router } from '@angular/router';
import { GetGenrePipe } from 'src/app/pipes/genrePipe/get-genre.pipe';
import { GetImagePipe } from 'src/app/pipes/imagePipe/get-image.pipe';

import { MovieCardComponent } from './movie-card.component';

describe('MovieCardComponent', () => {
  let component: MovieCardComponent;
  let fixture: ComponentFixture<MovieCardComponent>;
  let router: Router;
  let location: Location;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MovieCardComponent, GetGenrePipe, GetImagePipe ],
      imports: [MatCardModule],
      providers: []
    })
    .compileComponents();

    fixture = TestBed.createComponent(MovieCardComponent);
    router = TestBed.get(Router);
    location = TestBed.get(Location);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // it('it should navigate to detail', () => {
  //   component.gotoDetail(1);
  //   router.navigate(['']);
  //   expect(location.path()).toEqual('');
  // });

  it('it should create function for favorite movie', () => {
    const data = {
      favorite: false
    }
    component.item = data;
    component.onFavorite();
    fixture.detectChanges();
    expect(component.item.favorite).toBeTrue();
  });

  it('it should create function for favorite movie', () => {
    const data = {
      favorite: true
    }
    component.item = data;
    component.onFavorite();
    fixture.detectChanges();
    expect(component.item.favorite).toBeFalse();
  });
});
