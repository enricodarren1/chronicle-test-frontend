import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-movie-card',
  templateUrl: './movie-card.component.html',
  styleUrls: ['./movie-card.component.scss']
})
export class MovieCardComponent {
  @Input() item: any;
  @Input() genreList: any = [];
  @Input() source = '';

  constructor(
    private readonly router: Router
  ) {

  }

  gotoDetail(id: number) {
    this.router.navigate(['/movie-detail/' + id]);
  }

  onFavorite() {
    if (this.item?.favorite) {
      this.item.favorite = false;
    } else {
      this.item.favorite = true;
    }
  }
}
