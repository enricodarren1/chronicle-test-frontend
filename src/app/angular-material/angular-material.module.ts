import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatNativeDateModule } from '@angular/material/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

const angularMaterialModule = [
  MatCardModule,
  MatSelectModule,
  MatDatepickerModule,
  MatExpansionModule,
  MatNativeDateModule,
  MatButtonModule,
  MatIconModule,
  MatCardModule
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    angularMaterialModule
  ],
  exports: [angularMaterialModule]
})
export class AngularMaterialModule { }
